# exercise-gitlab-agent

# minikubeの準備

## 1. minikubeのdeploy
```
minikube start --force --driver=docker
```

## 2. minikubeにgitlab-agentsをdeployしておく
- GUI → Operate → Kubernets cluster → Connect a clusterより
```
helm repo add gitlab https://charts.gitlab.io
helm repo update
helm upgrade --install minikube-cluster gitlab/gitlab-agent \
    --namespace gitlab-agent-minikube-cluster \
    --create-namespace \
    --set image.tag=v17.0.0-rc1 \
    --set config.token=<token> \
    --set config.kasAddress=wss://kas.gitlab.com
```
- namespaceは変えたいので以下
```
helm repo add gitlab https://charts.gitlab.io
helm repo update
helm upgrade --install minikube-cluster gitlab/gitlab-agent \
    --namespace gitlab-agent \
    --create-namespace \
    --set image.tag=v17.0.0-rc1 \
    --set config.token=<token> \
    --set config.kasAddress=wss://kas.gitlab.com
```
- helmでdeploy
```
root@v0-dev-01:~/project/exercise-gitlab-agent# kubectl get pods -n gitlab-agent
NAME                                                READY   STATUS    RESTARTS   AGE
minikube-cluster-gitlab-agent-v2-5c66f9c65f-dvnzk   1/1     Running   0          17s
minikube-cluster-gitlab-agent-v2-5c66f9c65f-t6csx   1/1     Running   0          17s
```

# bare-metal-clusterの準備

## 1. bare-metalのk8sを構築しておく
- すでに構築済みなので省略
- clusterはk8sとminikubeの2つ
```
root@v0-dev-01:~/project/exercise-gitlab-agent# kubectl config get-clusters
NAME
kubernetes
minikube
```

## 2. bare-metal-clusterにgitlab-agentsをdeployしておく
- 同じ手順
```
helm repo add gitlab https://charts.gitlab.io
helm repo update
helm upgrade --install sample-baremetal-cluster gitlab/gitlab-agent \
    --namespace gitlab-agent \
    --create-namespace \
    --set image.tag=v17.0.0-rc1 \
    --set config.token=<token> \
    --set config.kasAddress=wss://kas.gitlab.com
```
- deployOK
    - baremetal-clusterは別pj用のもの
    - agentsをpj共有もできそうhttps://stackoverflow.com/questions/74036850/how-to-setup-one-gitlab-agent-for-all-projects-in-gitlab-group-to-deploy-project

```
root@v0-k8s-01:~# helm list -n gitlab-agent
NAME                    	NAMESPACE   	REVISION	UPDATED                                	STATUS  	CHART             	APP VERSION
baremetal-cluster       	gitlab-agent	1       	2024-04-24 06:36:02.620594486 +0000 UTC	deployed	gitlab-agent-2.0.0	v17.0.0-rc1
sample-baremetal-cluster	gitlab-agent	1       	2024-04-24 06:53:13.682572661 +0000 UTC	deployed	gitlab-agent-2.0.0	v17.0.0-rc1
```

# config.yamlを設定
- 以下を参考に設定
    - https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent/-/blob/master/doc/kubernetes_ci_access.md
    - https://stackoverflow.com/questions/74036850/how-to-setup-one-gitlab-agent-for-all-projects-in-gitlab-group-to-deploy-project
- 設定前(GUIで登録のみ) → default-configのまま
![img1](img/img1.png)
- 設定後(configをmainにpush) → configが読み込まれる
![img2](img/img2.png)

# .gitlab-ci.yamlを設定
- 一旦manualで生成

```
deploy-dev:
  extends: .base_helmfile
  environment: development
  stage: deploy
  script: 
    - kubectl config use-context 18la008c/exercise-gitlab-agent:minikube-cluster
    - helm install my-release oci://registry-1.docker.io/bitnamicharts/nginx
  when: manual

deploy-prd:
  extends: .base_helmfile
  environment: production
  stage: deploy
  script: 
    - kubectl config use-context 18la008c/exercise-gitlab-agent:sample-baremetal-cluster
    - helm install my-release oci://registry-1.docker.io/bitnamicharts/nginx
  when: manual
```

# GUIからmaunalでdeploy
- piplineからdeploy-devを選んで手動で実行
- ci-log
```
$ kubectl config use-context 18la008c/exercise-gitlab-agent:minikube-cluster
Switched to context "18la008c/exercise-gitlab-agent:minikube-cluster".

$ helm install my-release oci://registry-1.docker.io/bitnamicharts/nginx
WARNING: Kubernetes configuration file is group-readable. This is insecure. Location: /builds/18la008c/exercise-gitlab-agent.tmp/KUBECONFIG
WARNING: Kubernetes configuration file is world-readable. This is insecure. Location: /builds/18la008c/exercise-gitlab-agent.tmp/KUBECONFIG
Pulled: registry-1.docker.io/bitnamicharts/nginx:16.0.6
Digest: sha256:a47c2465ab440368df26b8bc84a9646659e20a4887311234f21f0e6ac758c744
NAME: my-release
LAST DEPLOYED: Wed Apr 24 07:18:29 2024
NAMESPACE: default
STATUS: deployed
REVISION: 1
TEST SUITE: None
NOTES:
CHART NAME: nginx
CHART VERSION: 16.0.6
APP VERSION: 1.25.5
```
- 手動で確認
    - deployできてる
```
root@v0-dev-01:~/project/exercise-gitlab-agent# kubectl get pods
NAME                               READY   STATUS    RESTARTS   AGE
my-release-nginx-5f5594b44-fq59l   1/1     Running   0          22s
```